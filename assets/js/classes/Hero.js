class Hero extends GameObject
{
    constructor()
    {
        super();

        if (Hero.instance) Hero.instance.destroy();
        Hero.instance = this;

        this.width = 32;
        this.height = 32;
        this.center = Stage.instance.center;
        this.speed = 4;
        this.canFire = true;
        this.fireRate = 250;
        this.lives = 3;
        this.reviveDelay = 2500;
        this.dead = false;

        this.reviveBound = this.revive.bind(this);
    }

    destroy()
    {
        if (this.dead) return false;

        --this.lives;

        if (this.lives >= 0) {
            this.dead = true;

            for (var i = EnemySpawn.instances.length - 1; i >= 0; i--) EnemySpawn.instances[i].paused = true;
            for (var i = Enemy.instances.length - 1; i >= 0; i--) Enemy.instances[i].destroy();

            setTimeout(this.reviveBound, this.reviveDelay);
        } else {
            super.destroy();

            Hero.instance = undefined;

            Game.instance.gameOver();
        }
    }

    update()
    {
        if (this.dead) return false;

        this.move();
        this.hitWalls();
        this.hitBounds();
        this.shoot();
    }

    render()
    {
        if (this.dead) return false;

        Stage.instance.context.fillStyle = '#00a6ef';
        Stage.instance.context.fillRect(this.left, this.top, this.width, this.height);
    }

    revive()
    {
        this.dead = false;

        this.center = Stage.instance.center;

        for (let spawn of EnemySpawn.instances) spawn.paused = false;
    }

    move()
    {
        if (Controller.isDown(Controller.W)) this.y -= this.speed;
        if (Controller.isDown(Controller.A)) this.x -= this.speed;
        if (Controller.isDown(Controller.S)) this.y += this.speed;
        if (Controller.isDown(Controller.D)) this.x += this.speed;
    }

    hitWalls()
    {
        for (let wall of Wall.instances) {
            let isColliding = this.isColliding(wall);

            switch (isColliding) {
                case 'top':
                    this.top = wall.bottom;
                    break;

                case 'left':
                    this.left = wall.right;
                    break;

                case 'bottom':
                    this.bottom = wall.top;
                    break;

                case 'right':
                    this.right = wall.left;
                    break;

                default:
                    break;
            }
        }
    }

    hitBounds()
    {
        if (this.top < Stage.instance.top) this.top = Stage.instance.top;
        if (this.left < Stage.instance.left) this.left = Stage.instance.left;
        if (this.bottom > Stage.instance.bottom) this.bottom = Stage.instance.bottom;
        if (this.right > Stage.instance.right) this.right = Stage.instance.right;
    }

    shoot()
    {
        if (!this.canFire || !Controller.isDown([Controller.ARROWUP, Controller.ARROWLEFT, Controller.ARROWDOWN, Controller.ARROWRIGHT])) return false;

        this.canFire = false;

        let bullet = new HeroBullet();
        bullet.center = this.center;

        if (Controller.isDown(Controller.ARROWUP)) bullet.speedY = -10;
        if (Controller.isDown(Controller.ARROWLEFT)) bullet.speedX = -10;
        if (Controller.isDown(Controller.ARROWDOWN)) bullet.speedY = 10;
        if (Controller.isDown(Controller.ARROWRIGHT)) bullet.speedX = 10;

        setTimeout(() => {
            this.canFire = true;
        }, this.fireRate);
    }

    get lives()
    {
        return this._lives;
    }

    set lives(value)
    {
        this._lives = value;

        for (let i = Heart.instances.length - 1; i >= 0; --i) Heart.instances[i].destroy();

        for (let i = 0; i < this.lives; ++i) new Heart();
    }
}

module.exports = Hero;
