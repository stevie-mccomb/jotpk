class EnemyBullet extends Bullet
{
    constructor()
    {
        super();

        EnemyBullet.instances.push(this);
    }

    destroy()
    {
        super.destroy();

        let index = EnemyBullet.instances.indexOf(this);

        if (index >= 0) EnemyBullet.instances.splice(index, 1);
    }
}

EnemyBullet.instances = [];

module.exports = EnemyBullet;
