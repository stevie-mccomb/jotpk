let mix = require('laravel-mix');

mix.options({
    publicPath: 'public',
    processCssUrls: false
});

mix.js('assets/js/app.js', 'public/js');

mix.sass('assets/sass/app.scss', 'public/css');
