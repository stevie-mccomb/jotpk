class Sprite
{
    constructor(owner, data)
    {
        this.owner = owner;
        this.data = data || {};

        this.image = new Image();
        this.image.src = data.src || '';

        this.ticks = 0;
        this.animationIndex = 0;
        this.frameIndex = 0;

        this.ticksPerFrame = data.ticksPerFrame || 15;

        this.loop = data.loop !== undefined ? data.loop : true;
        this.paused = data.paused !== undefined ? data.paused : false;
    }

    update()
    {
        if (this.paused) return false;

        ++this.ticks;

        if (this.ticks >= this.ticksPerFrame) {
            this.ticks = 0;

            ++this.frameIndex;

            if (!this.animation.frames[this.frameIndex]) {
                if (this.loop) {
                    this.frameIndex = 0;
                } else {
                    this.paused = true;
                }
            }
        }
    }

    render()
    {
        Stage.instance.context.drawImage(
            this.image,
            this.frame.x, // sub-region X
            this.frame.y, // sub-region Y
            this.frame.width, // sub-region width
            this.frame.height, // sub-region height
            this.owner.x, // x to draw to
            this.owner.y, // y to draw to
            this.frame.width, // width to draw
            this.frame.height // height to draw
        );
    }

    get animation()
    {
        return this.data.animations[this.animationIndex];
    }

    set animation(label)
    {
        if (label && this.animation && this.animation.label !== label && this.data.animations && this.data.animations.length) {
            for (let i = 0; i < this.data.animations.length; ++i) {
                if (this.data.animations[i].label === label) {
                    this.frameIndex = 0;
                    this.animationIndex = i;
                    break;
                }
            }
        }
    }

    get frame()
    {
        return this.animation.frames[this.frameIndex] ? this.animation.frames[this.frameIndex] : this.animation.frames[0];
    }
}

module.exports = Sprite;
