const jQuery = require('jquery');

export const W = 'w';
export const A = 'a';
export const S = 's';
export const D = 'd';
export const ARROWUP = 'arrowup';
export const ARROWLEFT = 'arrowleft';
export const ARROWDOWN = 'arrowdown';
export const ARROWRIGHT = 'arrowright';
export const DPAD_UP = 'd-pad-up';
export const DPAD_LEFT = 'd-pad-left';
export const DPAD_BOTTOM = 'd-pad-bottom';
export const DPAD_RIGHT = 'd-pad-right';
export const TRIANGLE = 'triangle';
export const SQUARE = 'square';
export const CIRCLE = 'circle';
export const CROSS = 'cross';
export const SELECT = 'select';
export const START = 'start';
export const L1 = 'l1';
export const L2 = 'l2';
export const L3 = 'l3';
export const R1 = 'r1';
export const R2 = 'r2';
export const R3 = 'r3';

export let inputs = [];

export function isUp(given) {
    if (typeof given === 'undefined' || typeof given === 'null') return false;
    if (typeof given !== 'object' && given.constructor.toString().indexOf('Array') < 0) given = [given];

    for (let input of given) {
        if (inputs.indexOf(input) < 0) return true;
    }

    return false;
}

export function isDown(given) {
    if (typeof given === 'undefined' || typeof given === 'null') return false;
    if (typeof given !== 'object' && given.constructor.toString().indexOf('Array') < 0) given = [given];

    for (let input of given) {
        if (inputs.indexOf(input) >= 0) return true;
    }

    return false;
}

jQuery(document).on('keydown', (e) => {
    let input = e.key.toLowerCase();
    let index = inputs.indexOf(input);

    if (index < 0) inputs.push(input);
});

jQuery(document).on('keyup', (e) => {
    let input = e.key.toLowerCase();
    let index = inputs.indexOf(input);

    if (index >= 0) inputs.splice(index, 1);
});
