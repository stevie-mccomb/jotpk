class HeroBullet extends Bullet
{
    constructor()
    {
        super();

        HeroBullet.instances.push(this);
    }

    destroy()
    {
        super.destroy();

        let index = HeroBullet.instances.indexOf(this);

        if (index >= 0) HeroBullet.instances.splice(index, 1);
    }
}

HeroBullet.instances = [];

module.exports = HeroBullet;
