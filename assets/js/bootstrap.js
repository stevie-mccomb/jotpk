window.jQuery = require('jquery');

window.Controller = require('./libraries/Controller');

window.GameObject = require('./classes/GameObject');
window.Sprite = require('./classes/Sprite');

window.Game = require('./classes/Game');
window.Stage = require('./classes/Stage');

window.Wall = require('./classes/Wall');

window.EnemySpawn = require('./classes/EnemySpawn');
window.Hero = require('./classes/Hero');
window.Enemy = require('./classes/Enemy');

window.Bullet = require('./classes/Bullet');
window.HeroBullet = require('./classes/HeroBullet');
window.EnemyBullet = require('./classes/EnemyBullet');

window.Heart = require('./classes/Heart');

if (!Number.prototype.snapTo) {
    Number.prototype.snapTo = function(value)
    {
        if (this > 0) return Math.ceil(value / this) * this;
        if (this < 0) return Math.floor(value / this) * this;
        return value;
    }
}
