class Stage
{
    constructor()
    {
        if (Stage.instance) Stage.instance.destroy();
        Stage.instance = this;

        this.element = jQuery('<canvas class="stage"></canvas>');
        Game.instance.element.append(this.element);

        this.canvas = this.element.get(0);
        this.context = this.canvas.getContext('2d');

        this.background = new Image();
        this.background.src = '/img/background.png';

        this.element.attr({
            width: this.width,
            height: this.height
        });
    }

    update()
    {
        //
    }

    render()
    {
        this.context.clearRect(this.left, this.top, this.width, this.height);
        this.context.drawImage(this.background, this.left, this.top, this.width, this.height);
    }

    destroy()
    {
        Stage.instance = undefined;

        this.element.remove();
    }

    get width()
    {
        return this.element.outerWidth();
    }

    get height()
    {
        return this.element.outerHeight();
    }

    get top()
    {
        return 0;
    }

    get left()
    {
        return 0;
    }

    get bottom()
    {
        return this.top + this.height;
    }

    get right()
    {
        return this.left + this.width;
    }

    get center()
    {
        return {
            x: this.width / 2,
            y: this.height / 2
        };
    }
}

module.exports = Stage;
