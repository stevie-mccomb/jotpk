class Heart extends GameObject
{
    constructor()
    {
        super();

        Heart.instances.push(this);

        let margin = 16;

        this.width = 32;
        this.height = 32;
        this.x = Heart.instances.length * (this.width + (margin / 4));
        this.y = margin;
        this.sprite = new Sprite(this, require('../sprites/Heart.json'));
    }

    destroy()
    {
        super.destroy();

        let index = Heart.instances.indexOf(this);

        if (index >= 0) Heart.instances.splice(index, 1);
    }
}

Heart.instances = [];

module.exports = Heart;
