class EnemySpawn extends GameObject
{
    constructor(x = 0, y = 0)
    {
        super();

        EnemySpawn.instances.push(this);

        this.width = 32;
        this.height = 32;
        this.x = x;
        this.y = y;
        this.minInterval = 2500;
        this.maxInterval = 10000;
        this.spawning = false;
        this.paused = false;

        this.timer = 0;

        this.spawnBound = this.spawn.bind(this);
    }

    destroy()
    {
        super.destroy();

        let index = EnemySpawn.instances.indexOf(this);

        if (index >= 0) EnemySpawn.instances.splice(index, 1);
    }

    update()
    {
        if (this.spawning || this.paused) return false;

        this.spawning = true;

        if (this.timer) clearTimeout(this.timer);

        this.timer = setTimeout(this.spawn.bind(this), this.randomInterval);
    }

    spawn()
    {
        if (this.paused) return false;

        new Enemy(this);

        this.spawning = false;
    }

    get randomInterval()
    {
        return Math.floor(Math.random() * (this.maxInterval - this.minInterval)) + this.minInterval;
    }
}

EnemySpawn.instances = [];

module.exports = EnemySpawn;
