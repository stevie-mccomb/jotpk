class GameObject
{
    constructor()
    {
        GameObject.instances.push(this);

        this.x = 0;
        this.y = 0;
        this.width = 0;
        this.height = 0;
    }

    update()
    {
        //
    }

    render()
    {
        if (this.sprite) {
            this.sprite.update();
            this.sprite.render();
        }
    }

    destroy()
    {
        let index = GameObject.instances.indexOf(this);

        if (index >= 0) GameObject.instances.splice(index, 1);
    }

    isColliding(other)
    {
        let isColliding = (
            this.top < other.bottom
                &&
            this.bottom > other.top
                &&
            this.left < other.right
                &&
            this.right > other.left
        );

        if (!isColliding) return false;

        let distanceTops = Math.abs(this.top - other.bottom);
        let distanceLefts = Math.abs(this.left - other.right);
        let distanceBottoms = Math.abs(this.bottom - other.top);
        let distanceRights = Math.abs(this.right - other.left);

        let shallowest = Math.min(distanceTops, distanceLefts, distanceBottoms, distanceRights);

        if (shallowest === distanceTops) return 'top';
        if (shallowest === distanceLefts) return 'left';
        if (shallowest === distanceBottoms) return 'bottom';
        if (shallowest === distanceRights) return 'right';
    }

    get top()
    {
        return this.y;
    }

    get left()
    {
        return this.x;
    }

    get bottom()
    {
        return this.y + this.height;
    }

    get right()
    {
        return this.x + this.width;
    }

    get center()
    {
        return {
            x: this.left + (this.width / 2),
            y: this.top + (this.height / 2)
        };
    }

    set top(value)
    {
        this.y = value;
    }

    set left(value)
    {
        this.x = value;
    }

    set bottom(value)
    {
        this.y = value - this.height;
    }

    set right(value)
    {
        this.x = value - this.width;
    }

    set center(pos) {
        this.x = pos.x - (this.width / 2);
        this.y = pos.y - (this.height / 2);
    }
}

GameObject.instances = [];

module.exports = GameObject;
