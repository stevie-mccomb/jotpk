class Bullet extends GameObject
{
    constructor()
    {
        super();

        Bullet.instances.push(this);

        this.speedX = 0;
        this.speedY = 0;
        this.color = '#e00';
        this.width = 8;
        this.height = 8;
    }

    update()
    {
        this.x += this.speedX;
        this.y += this.speedY;

        if (this.top > Stage.instance.bottom) this.destroy();
        if (this.left > Stage.instance.right) this.destroy();
        if (this.right < Stage.instance.left) this.destroy();
        if (this.bottom < Stage.instance.top) this.destroy();
    }

    render()
    {
        Stage.instance.context.fillStyle = this.color;
        Stage.instance.context.fillRect(this.left, this.top, this.width, this.height);
    }

    destroy()
    {
        super.destroy();

        let index = Bullet.instances.indexOf(this);

        if (index >= 0) Bullet.instances.splice(index, 1);
    }
}

Bullet.instances = [];

module.exports = Bullet;
