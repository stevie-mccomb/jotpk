class Game
{
    constructor(el)
    {
        if (Game.instance) Game.instance.destroy();
        Game.instance = this;

        this.element = el;
        this.scoreboard = jQuery('<div class="scoreboard"></div>');
        this.element.append(this.scoreboard);

        this.score = 0;

        for (var i = Bullet.instances.length - 1; i >= 0; --i) Bullet.instances[i].destroy();
        for (var i = Enemy.instances.length - 1; i >= 0; --i) Enemy.instances[i].destroy();
        for (var i = EnemySpawn.instances.length - 1; i >= 0; --i) EnemySpawn.instances[i].destroy();
        for (var i = Wall.instances.length - 1; i >= 0; --i) Wall.instances[i].destroy();

        new Stage();

        new Wall(0, 0, 32 * 8, 32);
        new Wall(0, 0, 32, 32 * 8);
        new Wall(0, Stage.instance.bottom - 32, 32 * 8, 32);
        new Wall(0, Stage.instance.bottom - (32 * 8), 32, 32 * 8);
        new Wall(Stage.instance.right - (32 * 8), 0, 32 * 8, 32);
        new Wall(Stage.instance.right - 32, 0, 32, 32 * 8);
        new Wall(Stage.instance.right - (32 * 8), Stage.instance.bottom - 32, 32 * 8, 32);
        new Wall(Stage.instance.right - 32, Stage.instance.bottom - (32 * 8), 32, 32 * 8);

        // Top lane
        new EnemySpawn(32 * 8, 0);
        new EnemySpawn(32 * 9, 0);
        new EnemySpawn(32 * 10, 0);
        new EnemySpawn(32 * 11, 0);

        // Left lane
        new EnemySpawn(0, 32 * 8);
        new EnemySpawn(0, 32 * 9);
        new EnemySpawn(0, 32 * 10);
        new EnemySpawn(0, 32 * 11);

        // Bottom lane
        new EnemySpawn(32 * 8, Stage.instance.bottom - 32);
        new EnemySpawn(32 * 9, Stage.instance.bottom - 32);
        new EnemySpawn(32 * 10, Stage.instance.bottom - 32);
        new EnemySpawn(32 * 11, Stage.instance.bottom - 32);

        // Right lane
        new EnemySpawn(Stage.instance.bottom - 32, 32 * 8);
        new EnemySpawn(Stage.instance.bottom - 32, 32 * 9);
        new EnemySpawn(Stage.instance.bottom - 32, 32 * 10);
        new EnemySpawn(Stage.instance.bottom - 32, 32 * 11);

        new Hero();

        this.loopBound = this.loop.bind(this);

        this.animationFrame = requestAnimationFrame(this.loopBound);
    }

    loop()
    {
        if (this.paused) return false;

        this.update();
        this.render();

        this.animationFrame = requestAnimationFrame(this.loopBound);
    }

    update()
    {
        Stage.instance.update();

        for (let obj of GameObject.instances) obj.update();
    }

    render()
    {
        Stage.instance.render();

        for (let obj of GameObject.instances) obj.render();
    }

    destroy()
    {
        Game.instance = undefined;

        cancelAnimationFrame(this.animationFrame);

        this.scoreboard.remove();
    }

    gameOver()
    {
        this.paused = true;

        let element = jQuery('<div class="game-over"></div>');
        this.element.append(element);

        element.text('Game Over!');

        element.on('click', () => {
            new Game(this.element);

            element.remove();
        });
    }

    get score()
    {
        return this._score;
    }

    set score(value)
    {
        this._score = value;

        this.scoreboard.text('Score: ' + this.score);
    }
}

module.exports = Game;
