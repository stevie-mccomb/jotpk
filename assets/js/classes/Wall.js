class Wall extends GameObject
{
    constructor(x = 0, y = 0, width = 0, height = 0)
    {
        super();

        Wall.instances.push(this);

        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
    }
}

Wall.instances = [];

module.exports = Wall;
