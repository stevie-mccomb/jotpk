class Enemy extends GameObject
{
    constructor(spawn)
    {
        super();

        Enemy.instances.push(this);

        this.width = 32;
        this.height = 32;
        this.center = spawn.center;
        this.color = '#a0e';
        this.speed = 2;
    }

    destroy()
    {
        super.destroy();

        let index = Enemy.instances.indexOf(this);

        if (index >= 0) Enemy.instances.splice(index, 1);
    }

    update()
    {
        this.move();
        this.hitWalls();
        this.hitEnemies();
        this.hitHero();
        this.hitBullets();
    }

    render()
    {
        Stage.instance.context.fillStyle = this.color;
        Stage.instance.context.fillRect(this.left, this.top, this.width, this.height);
    }

    move()
    {
        if (!Hero.instance) return false;

        let target = Hero.instance.center;
        let current = this.center;
        let direction = Math.atan2(target.y - current.y, target.x - current.x) * (180 / Math.PI);

        this.center = {
            x: current.x + Math.cos(direction * (Math.PI / 180)) * this.speed,
            y: current.y + Math.sin(direction * (Math.PI / 180)) * this.speed
        };
    }

    hitWalls()
    {
        for (let wall of Wall.instances) {
            switch (this.isColliding(wall)) {
                case 'top':
                    this.top = wall.bottom;
                    break;

                case 'left':
                    this.left = wall.right;
                    break;

                case 'bottom':
                    this.bottom = wall.top;
                    break;

                case 'right':
                    this.right = wall.left;
                    break;

                default:
                    break;
            }
        }
    }

    hitEnemies()
    {
        for (let enemy of Enemy.instances) {
            if (enemy == this) continue;

            switch (this.isColliding(enemy)) {
                case 'top':
                    this.top = enemy.bottom;
                    break;

                case 'left':
                    this.left = enemy.right;
                    break;

                case 'bottom':
                    this.bottom = enemy.top;
                    break;

                case 'right':
                    this.right = enemy.left;
                    break;

                default:
                    break;
            }
        }
    }

    hitHero()
    {
        if (!Hero.instance) return false;

        if (this.isColliding(Hero.instance)) {
            Hero.instance.destroy();
            this.destroy();
        }
    }

    hitBullets()
    {
        for (let bullet of HeroBullet.instances) {
            if (this.isColliding(bullet)) {
                ++Game.instance.score;

                bullet.destroy();
                this.destroy();
            }
        }
    }
}

Enemy.instances = [];

module.exports = Enemy;
